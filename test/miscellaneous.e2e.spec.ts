import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { TestModule } from '../src/test.module';
import { DatabaseService } from '../src/database/database.service';
import { INTERNAL_SERVER_ERROR } from '../src/constants/errors.constants';
import { getAccessToken, register } from '../src/utils/test/auth.test.utils';
import {
  ANIME_INPUT_EXAMPLE,
  REGISTER_INPUT_EXAMPLE1,
  LOGIN_INPUT_EXAMPLE1,
  SONG_INPUT_EXAMPLE,
} from '../src/constants/test.constants';

import { addAnime } from '../src/utils/test/anime.test.utils';
import { TestLogger } from '../src/utils/test.logger';
import { addSong } from '../src/utils/test/song.test.utils';

describe('Anime e2e', () => {
  let app: INestApplication;
  let dbService: DatabaseService;
  let moduleFixture: TestingModule;
  let access_token: string;

  beforeAll(async () => {
    jest.setTimeout(60000);
    moduleFixture = await Test.createTestingModule({
      imports: [TestModule],
    }).compile();
    app = moduleFixture.createNestApplication();
    await app.init();
    app.useLogger(moduleFixture.get<TestLogger>(TestLogger));
    dbService = moduleFixture.get<DatabaseService>(DatabaseService);
    await dbService.clearDB();
    await register(REGISTER_INPUT_EXAMPLE1, request, app);
  });

  beforeEach(async () => {
    access_token = await getAccessToken(LOGIN_INPUT_EXAMPLE1, request, app);
  });

  afterAll(async () => {
    dbService = moduleFixture.get<DatabaseService>(DatabaseService);
    await dbService.clearDB();
    await app.close();
  });

  const gql = '/graphql';

  describe('complexity', () => {
    it('should avoid deep nested object featching', async () => {
      const animeId = await addAnime(
        ANIME_INPUT_EXAMPLE,
        access_token,
        request,
        app,
      );
      const song_example = {
        ...SONG_INPUT_EXAMPLE,
        animeId: animeId,
      };
      await addSong(song_example, access_token, request, app);
      return request(app.getHttpServer())
        .post(gql)
        .send({
          query: `
          query 
          {
            anime(animeId: "${animeId}")
            {
              animeId
              songs {
                anime {
                  songs {
                    anime{
                      songs{
                        anime {
                          songs {
                            anime {
                              songs {
                                anime{
                                  songs{
                                    anime {
                                      songs {
                                        anime {
                                          songs {
                                            anime{
                                              songs{
                                                anime {
                                                  songs {
                                                    anime {
                                                      songs {
                                                        anime{
                                                          songs{
                                                            animeId
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }`,
        })
        .expect((res) => {
          expect(res.body.errors).toContainEqual({
            message: INTERNAL_SERVER_ERROR.COMPLEXITY_ERROR,
            statusCode: 500,
          });
        });
    });
  });
});
