import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { TestModule } from '../src/test.module';
import { DatabaseService } from '../src/database/database.service';
import { BAD_USER_INPUT } from '../src/constants/errors.constants';
import { getAccessToken, register } from '../src/utils/test/auth.test.utils';
import {
  ANIME_INPUT_EXAMPLE,
  REGISTER_INPUT_EXAMPLE1,
  LOGIN_INPUT_EXAMPLE1,
} from '../src/constants/test.constants';

import { addAnime } from '../src/utils/test/anime.test.utils';
import { TestLogger } from '../src/utils/test.logger';

describe('Anime e2e', () => {
  let app: INestApplication;
  let dbService: DatabaseService;
  let moduleFixture: TestingModule;
  let access_token: string;

  beforeAll(async () => {
    jest.setTimeout(60000);
    moduleFixture = await Test.createTestingModule({
      imports: [TestModule],
    }).compile();
    app = moduleFixture.createNestApplication();
    await app.init();
    app.useLogger(moduleFixture.get<TestLogger>(TestLogger));
    dbService = moduleFixture.get<DatabaseService>(DatabaseService);
    await dbService.clearDB();
    await register(REGISTER_INPUT_EXAMPLE1, request, app);
  });

  beforeEach(async () => {
    access_token = await getAccessToken(LOGIN_INPUT_EXAMPLE1, request, app);
  });

  afterAll(async () => {
    dbService = moduleFixture.get<DatabaseService>(DatabaseService);
    await dbService.clearDB();
    await app.close();
  });

  const gql = '/graphql';

  describe('createAnime', () => {
    it('should create a new anime', () => {
      return request(app.getHttpServer())
        .post(gql)
        .set('Authorization', `Bearer ${access_token}`)
        .send({
          query: `
          mutation 
          {
            createAnime(createAnimeInput:
            {
              romajiName: "Kimetsu no Yaiba: Yūkaku-hen"
              englishName: "Demon Slayer: Kimetsu no Yaiba Entertainment District Arc"
              annId: 24043
            })
            {
              animeId
              romajiName
              englishName
              annId
              anilistId
              malId
            }
          }`,
        })
        .expect(200)
        .expect((res) => {
          expect(res.body.data.createAnime).toEqual(
            expect.objectContaining({
              animeId: expect.any(String),
              romajiName: 'Kimetsu no Yaiba: Yūkaku-hen',
              englishName:
                'Demon Slayer: Kimetsu no Yaiba Entertainment District Arc',
              annId: 24043,
              anilistId: null,
              malId: null,
            }),
          );
        });
    });

    it('should return error for missing arguments', () => {
      return request(app.getHttpServer())
        .post(gql)
        .send({
          query: `
            mutation 
            {
              createAnime(createAnimeInput:
              {
                romajiName: "Kimetsu no Yaiba: Yūkaku-hen"
                englishName: "Demon Slayer: Kimetsu no Yaiba Entertainment District Arc"
              })
              {
                animeId
                romajiName
                englishName
                annId
                anilistId
                malId
              }
            }`,
        })
        .expect((res) => {
          expect(res.body.errors).toContainEqual({
            message: 'Your query is not valid.',
            statusCode: 400,
          });
        });
    });
  });

  it('should return error for incorrect type', () => {
    return request(app.getHttpServer())
      .post(gql)
      .send({
        query: `
          mutation 
          {
            createAnime(createAnimeInput:
            {
              romajiName: "Kimetsu no Yaiba: Yūkaku-hen"
              englishName: "Demon Slayer: Kimetsu no Yaiba Entertainment District Arc"
              annId: "24043"
            })
            {
              animeId
              romajiName
              englishName
              annId
              anilistId
              malId
            }
          }`,
      })
      .expect((res) => {
        expect(res.body.errors).toContainEqual({
          message: 'Your query is not valid.',
          statusCode: 400,
        });
      });
  });

  describe('queryAnime', () => {
    it('should retrieve an anime using id', async () => {
      const animeId = await addAnime(
        ANIME_INPUT_EXAMPLE,
        access_token,
        request,
        app,
      );
      return request(app.getHttpServer())
        .post(gql)
        .send({
          query: `
          query 
          {
            anime(animeId: "${animeId}")
            {
              animeId
              romajiName
              englishName
              annId
            }
          }`,
        })
        .expect((res) => {
          expect(res.body.data.anime).toEqual(
            expect.objectContaining({
              animeId: expect.any(String),
              romajiName: 'Kimetsu no Yaiba: Yūkaku-hen',
              englishName:
                'Demon Slayer: Kimetsu no Yaiba Entertainment District Arc',
              annId: 24043,
            }),
          );
        });
    });
    it('should return error for anime not found', async () => {
      return request(app.getHttpServer())
        .post(gql)
        .send({
          query: `
          query 
          {
            anime(animeId: "00000000-0000-0000-0000-000000000000")
            {
              animeId
              romajiName
              englishName
              annId
            }
          }`,
        })
        .expect((res) => {
          expect(res.body.errors).toContainEqual({
            message: 'Anime not found.',
            statusCode: 400,
          });
        });
    });

    it('should return error for bad syntax', async () => {
      return request(app.getHttpServer())
        .post(gql)
        .send({
          query: `
          query 
          {
            anime(animeId: "00000000-0000-0000-0000-000000000000")
            {
              songs
            }
          }`,
        })
        .expect((res) => {
          expect(res.body.errors).toContainEqual({
            message: 'Your query is not valid.',
            statusCode: 400,
          });
        });
    });
  });

  describe('updateAnime', () => {
    it('should update an anime', async () => {
      const animeId = await addAnime(
        ANIME_INPUT_EXAMPLE,
        access_token,
        request,
        app,
      );
      return request(app.getHttpServer())
        .post(gql)
        .set('Authorization', `Bearer ${access_token}`)
        .send({
          query: `
          mutation 
            {
              updateAnime(updateAnimeInput:
              {
                animeId: "${animeId}"
                romajiName: "Kimetsu no Yaiba: Yūkaku-hen"
                annId: 24043
              })
              {
                animeId
                romajiName
                englishName
                annId
                anilistId
                malId
              }
          }`,
        })
        .expect((res) => {
          expect(res.body.data.updateAnime).toEqual(
            expect.objectContaining({
              animeId: expect.any(String),
              romajiName: 'Kimetsu no Yaiba: Yūkaku-hen',
              englishName:
                'Demon Slayer: Kimetsu no Yaiba Entertainment District Arc',
              annId: 24043,
              anilistId: null,
              malId: null,
            }),
          );
        });
    });

    it('should return an error for bad uuid', async () => {
      return request(app.getHttpServer())
        .post(gql)
        .set('Authorization', `Bearer ${access_token}`)
        .send({
          query: `
          mutation 
            {
              updateAnime(updateAnimeInput:
              {
                animeId: "00000000-0000-0000-0000-000000000000"
                romajiName: "Kimetsu no Yaiba: Yūkaku-hen"
                annId: 24043
              })
              {
                animeId
                romajiName
                englishName
                annId
                anilistId
                malId
              }
          }`,
        })
        .expect((res) => {
          expect(res.body.errors).toContainEqual({
            message: BAD_USER_INPUT.ANIME_NOT_FOUND,
            statusCode: 400,
          });
        });
    });
  });

  describe('removeAnime', () => {
    it('should remove an anime', async () => {
      const animeId = await addAnime(
        ANIME_INPUT_EXAMPLE,
        access_token,
        request,
        app,
      );
      await request(app.getHttpServer())
        .post(gql)
        .set('Authorization', `Bearer ${access_token}`)
        .send({
          query: `
          mutation 
            {
              removeAnime(animeId: "${animeId}")
              {
                animeId
                romajiName
                englishName
                annId
                anilistId
                malId
              }
          }`,
        });
      return request(app.getHttpServer())
        .post(gql)
        .send({
          query: `
          query 
            {
              anime(animeId: "${animeId}")
              {
                animeId
              }
          }`,
        })
        .expect((res) => {
          expect(res.body.errors).toContainEqual({
            message: BAD_USER_INPUT.ANIME_NOT_FOUND,
            statusCode: 400,
          });
        });
    });

    it('should return an error for bad uuid', async () => {
      return request(app.getHttpServer())
        .post(gql)
        .set('Authorization', `Bearer ${access_token}`)
        .send({
          query: `
          mutation 
            {
              removeAnime(animeId: "00000000-0000-0000-0000-000000000000")
              {
                animeId
              }
          }`,
        })
        .expect((res) => {
          expect(res.body.errors).toContainEqual({
            message: BAD_USER_INPUT.ANIME_NOT_FOUND,
            statusCode: 400,
          });
        });
    });
  });
});
