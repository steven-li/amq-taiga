import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { TestModule } from '../src/test.module';
import { DatabaseService } from '../src/database/database.service';
import { getAccessToken, register } from '../src/utils/test/auth.test.utils';
import { TestLogger } from '../src/utils/test.logger';
import {
  UNAUTHORIZED,
  BAD_USER_INPUT,
} from '../src/constants/errors.constants';
describe('Auth e2e', () => {
  let app: INestApplication;
  let dbService: DatabaseService;
  let moduleFixture: TestingModule;

  beforeAll(async () => {
    jest.setTimeout(60000);
    moduleFixture = await Test.createTestingModule({
      imports: [TestModule],
    }).compile();
    app = moduleFixture.createNestApplication();
    await app.init();
    app.useLogger(moduleFixture.get<TestLogger>(TestLogger));
    dbService = moduleFixture.get<DatabaseService>(DatabaseService);
    await dbService.clearDB();
  });

  afterAll(async () => {
    dbService = moduleFixture.get<DatabaseService>(DatabaseService);
    await dbService.clearDB();
    await app.close();
  });

  const gql = '/graphql';

  describe('userRegister', () => {
    it('should register a new user', () => {
      return request(app.getHttpServer())
        .post(gql)
        .send({
          query: `
          mutation{
            register(registerUserInput: {
              email: "test@test.com"
              pseudo: "test"
              password: "password"
            }){
              email
              pseudo
              role
            }
          }`,
        })
        .expect((res) => {
          expect(res.body.data.register).toEqual(
            expect.objectContaining({
              email: 'test@test.com',
              pseudo: 'test',
              role: 'USER',
            }),
          );
        });
    });
    it('should return a error for existing account', () => {
      return request(app.getHttpServer())
        .post(gql)
        .send({
          query: `
          mutation{
            register(registerUserInput: {
              email: "test@test.com"
              pseudo: "test"
              password: "password"
            }){
              email
              pseudo
              role
            }
          }`,
        })
        .expect((res) => {
          expect(res.body.errors).toContainEqual({
            message: BAD_USER_INPUT.EMAIL_IN_USE,
            statusCode: 400,
          });
        });
    });
  });

  describe('userLogin', () => {
    it('should login a user', async () => {
      await register(
        {
          email: 'test2@test.com',
          pseudo: 'test',
          password: 'password',
        },
        request,
        app,
      );
      return request(app.getHttpServer())
        .post(gql)
        .send({
          query: `
          mutation {
            login(loginUserInput: 
            {
              email: "test2@test.com",
              password: "password"
            })
            {
              access_token
              user {
                email
              }
            }
          }`,
        })
        .expect((res) => {
          expect(res.body.data.login).toEqual(
            expect.objectContaining({
              access_token: expect.any(String),
              user: {
                email: 'test2@test.com',
              },
            }),
          );
        });
    });
  });

  describe('Authorization', () => {
    it('should return an unauthorized error', () => {
      return request(app.getHttpServer())
        .post(gql)
        .send({
          query: `
          mutation 
          {
            createAnime(createAnimeInput:
            {
              romajiName: "Kimetsu no Yaiba: Yūkaku-hen"
              englishName: "Demon Slayer: Kimetsu no Yaiba Entertainment District Arc"
              annId: 24043
            })
            {
              animeId
            }
          }`,
        })
        .expect((res) => {
          expect(res.body.errors).toContainEqual({
            message: UNAUTHORIZED.UNAUTHORIZED,
            statusCode: 401,
          });
        });
    });

    it('should allow user to create an anime', async () => {
      const access_token = await getAccessToken(
        {
          email: 'test2@test.com',
          password: 'password',
        },
        request,
        app,
      );
      return request(app.getHttpServer())
        .post(gql)
        .set('Authorization', `Bearer ${access_token}`)
        .send({
          query: `
          mutation 
          {
            createAnime(createAnimeInput:
            {
              romajiName: "Kimetsu no Yaiba: Yūkaku-hen"
              englishName: "Demon Slayer: Kimetsu no Yaiba Entertainment District Arc"
              annId: 24043
            })
            {
              animeId
              romajiName
              englishName
              annId
              anilistId
              malId
            }
          }`,
        })
        .expect((res) => {
          expect(res.body.data.createAnime).toEqual(
            expect.objectContaining({
              animeId: expect.any(String),
              romajiName: 'Kimetsu no Yaiba: Yūkaku-hen',
              englishName:
                'Demon Slayer: Kimetsu no Yaiba Entertainment District Arc',
              annId: 24043,
              anilistId: null,
              malId: null,
            }),
          );
        });
    });
  });
});
