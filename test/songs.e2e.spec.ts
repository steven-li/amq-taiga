import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { TestModule } from '../src/test.module';
import { DatabaseService } from '../src/database/database.service';
import { getAccessToken, register } from '../src/utils/test/auth.test.utils';
import {
  ANIME_INPUT_EXAMPLE,
  REGISTER_INPUT_EXAMPLE1,
  LOGIN_INPUT_EXAMPLE1,
} from '../src/constants/test.constants';

import { addAnime } from '../src/utils/test/anime.test.utils';
import { addSong } from '../src/utils/test/song.test.utils';
import { SONG_INPUT_EXAMPLE } from '../src/constants/test.constants';
import { TestLogger } from '../src/utils/test.logger';
import { BAD_USER_INPUT } from '../src/constants/errors.constants';

describe('Song e2e', () => {
  let app: INestApplication;
  let dbService: DatabaseService;
  let moduleFixture: TestingModule;
  let access_token: string;

  beforeAll(async () => {
    jest.setTimeout(60000);
    moduleFixture = await Test.createTestingModule({
      imports: [TestModule],
    }).compile();
    app = moduleFixture.createNestApplication();
    await app.init();
    app.useLogger(moduleFixture.get<TestLogger>(TestLogger));
    dbService = moduleFixture.get<DatabaseService>(DatabaseService);
    await dbService.clearDB();
    await register(REGISTER_INPUT_EXAMPLE1, request, app);
  });

  beforeEach(async () => {
    access_token = await getAccessToken(LOGIN_INPUT_EXAMPLE1, request, app);
  });

  afterAll(async () => {
    dbService = moduleFixture.get<DatabaseService>(DatabaseService);
    await dbService.clearDB();
    await app.close();
  });

  const gql = '/graphql';

  describe('createSong', () => {
    it('should create a song anime', async () => {
      const animeId = await addAnime(
        ANIME_INPUT_EXAMPLE,
        access_token,
        request,
        app,
      );
      return request(app.getHttpServer())
        .post(gql)
        .set('Authorization', `Bearer ${access_token}`)
        .send({
          query: `
          mutation 
          {
            createSong(createSongInput:
            {
              animeId: "${animeId}",
              type: OP
              title: "Zankyou Zanka",
              artist: "Aimer"
            })
            {
              songId
              animeId
              title
              artist
              type
              anime {
                romajiName
              }
            }
          }`,
        })
        .expect(200)
        .expect((res) => {
          expect(res.body.data.createSong).toEqual(
            expect.objectContaining({
              animeId: expect.any(String),
              songId: expect.any(String),
              title: 'Zankyou Zanka',
              artist: 'Aimer',
              type: 'OP',
              anime: {
                romajiName: 'Kimetsu no Yaiba: Yūkaku-hen',
              },
            }),
          );
        });
    });
  });

  describe('querySong', () => {
    it('should retrive song information', async () => {
      const animeId = await addAnime(
        ANIME_INPUT_EXAMPLE,
        access_token,
        request,
        app,
      );
      const song_example = {
        ...SONG_INPUT_EXAMPLE,
        animeId: animeId,
      };
      const songId = await addSong(song_example, access_token, request, app);
      return request(app.getHttpServer())
        .post(gql)
        .send({
          query: `
          query 
          {
            song(songId: "${songId}")
            {
              animeId
              songId
              title
              type
              artist
            }
          }`,
        })
        .expect((res) => {
          expect(res.body.data.song).toEqual(
            expect.objectContaining({
              animeId: expect.any(String),
              songId: expect.any(String),
              title: SONG_INPUT_EXAMPLE.title,
              type: SONG_INPUT_EXAMPLE.type,
              artist: SONG_INPUT_EXAMPLE.artist,
            }),
          );
        });
    });
  });

  describe('songUpdate', () => {
    it('should update a song', async () => {
      const animeId = await addAnime(
        ANIME_INPUT_EXAMPLE,
        access_token,
        request,
        app,
      );
      const song_example = {
        ...SONG_INPUT_EXAMPLE,
        animeId: animeId,
      };
      const songId = await addSong(song_example, access_token, request, app);
      return request(app.getHttpServer())
        .post(gql)
        .set('Authorization', `Bearer ${access_token}`)
        .send({
          query: `
          mutation
            {
              updateSong(updateSongInput:
              {
                songId: "${songId}",
                type: OP,
                artist: "Aimer",
              })
              {
                songId
                animeId
                title
                artist
                type
                anime {
                  romajiName
                }
              }
          }`,
        })
        .expect(200)
        .expect((res) => {
          expect(res.body.data.updateSong).toEqual(
            expect.objectContaining({
              animeId: expect.any(String),
              songId: expect.any(String),
              title: 'Zankyou Zanka',
              artist: 'Aimer',
              type: 'OP',
              anime: {
                romajiName: 'Kimetsu no Yaiba: Yūkaku-hen',
              },
            }),
          );
        });
    });
  });

  describe('removeSong', () => {
    it('should remove an song', async () => {
      const animeId = await addAnime(
        ANIME_INPUT_EXAMPLE,
        access_token,
        request,
        app,
      );
      const song_example = {
        ...SONG_INPUT_EXAMPLE,
        animeId: animeId,
      };
      const songId = await addSong(song_example, access_token, request, app);
      request(app.getHttpServer())
        .post(gql)
        .set('Authorization', `Bearer ${access_token}`)
        .send({
          query: `
          mutation 
            {
              removeSong(songId: "${songId}")
              {
                songId
              }
          }`,
        })
        .expect(200);
      request(app.getHttpServer())
        .post(gql)
        .send({
          query: `
          query 
            {
              song(songId: "${songId}")
              {
                songId
              }
          }`,
        })
        .expect(400);
    });
    it('should remove songs by deleting an anime', async () => {
      const animeId = await addAnime(
        ANIME_INPUT_EXAMPLE,
        access_token,
        request,
        app,
      );
      const song_example = {
        ...SONG_INPUT_EXAMPLE,
        animeId: animeId,
      };
      const songId = await addSong(song_example, access_token, request, app);
      await request(app.getHttpServer())
        .post(gql)
        .set('Authorization', `Bearer ${access_token}`)
        .send({
          query: `
          mutation 
            {
              removeAnime(animeId: "${animeId}")
              {
                animeId
              }
          }`,
        });
      return request(app.getHttpServer())
        .post(gql)
        .send({
          query: `
          query 
            {
              song(songId: "${songId}")
              {
                songId
              }
          }`,
        })
        .expect((res) => {
          expect(res.body.errors).toContainEqual({
            message: BAD_USER_INPUT.SONG_NOT_FOUND,
            statusCode: 400,
          });
        });
    });
  });
});
