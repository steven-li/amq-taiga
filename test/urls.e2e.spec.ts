import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { TestModule } from '../src/test.module';
import { DatabaseService } from '../src/database/database.service';
import { getAccessToken, register } from '../src/utils/test/auth.test.utils';
import {
  ANIME_INPUT_EXAMPLE,
  SONG_INPUT_EXAMPLE,
  URL_INPUT_EXAMPLE,
  REGISTER_INPUT_EXAMPLE1,
  LOGIN_INPUT_EXAMPLE1,
} from '../src/constants/test.constants';

import { addAnime } from '../src/utils/test/anime.test.utils';
import { addSong } from '../src/utils/test/song.test.utils';
import { addUrl } from '../src/utils/test/url.test.utils';
import { BAD_USER_INPUT } from '../src/constants/errors.constants';
import { TestLogger } from '../src/utils/test.logger';

describe('Song e2e', () => {
  let app: INestApplication;
  let dbService: DatabaseService;
  let moduleFixture: TestingModule;
  let access_token: string;

  beforeAll(async () => {
    jest.setTimeout(60000);
    moduleFixture = await Test.createTestingModule({
      imports: [TestModule],
    }).compile();
    app = moduleFixture.createNestApplication();
    await app.init();
    app.useLogger(moduleFixture.get<TestLogger>(TestLogger));
    dbService = moduleFixture.get<DatabaseService>(DatabaseService);
    await dbService.clearDB();
    await register(REGISTER_INPUT_EXAMPLE1, request, app);
  });

  beforeEach(async () => {
    access_token = await getAccessToken(LOGIN_INPUT_EXAMPLE1, request, app);
  });

  afterAll(async () => {
    dbService = moduleFixture.get<DatabaseService>(DatabaseService);
    await dbService.clearDB();
    await app.close();
  });

  const gql = '/graphql';

  describe('createUrl', () => {
    it('should create a url object', async () => {
      const animeId = await addAnime(
        ANIME_INPUT_EXAMPLE,
        access_token,
        request,
        app,
      );
      const song_example = {
        ...SONG_INPUT_EXAMPLE,
        animeId: animeId,
      };
      const songId = await addSong(song_example, access_token, request, app);
      return request(app.getHttpServer())
        .post(gql)
        .set('Authorization', `Bearer ${access_token}`)
        .send({
          query: `
          mutation 
          {
            createUrl(createUrlInput:
            {
              songId: "${songId}",
              name: "catbox_720"
              link: "https://files.catbox.moe/rrljjp.webm",
            })
            {
              urlId
              songId
              name
              link
            }
          }`,
        })
        .expect((res) => {
          expect(res.body.data.createUrl).toEqual(
            expect.objectContaining({
              urlId: expect.any(String),
              songId: expect.any(String),
              link: 'https://files.catbox.moe/rrljjp.webm',
              name: 'catbox_720',
            }),
          );
        });
    });
  });
  describe('updateUrl', () => {
    it('should update an url object', async () => {
      const animeId = await addAnime(
        ANIME_INPUT_EXAMPLE,
        access_token,
        request,
        app,
      );
      const song_example = {
        ...SONG_INPUT_EXAMPLE,
        animeId: animeId,
      };
      const songId = await addSong(song_example, access_token, request, app);
      const url_example = {
        ...URL_INPUT_EXAMPLE,
        songId: songId,
      };
      const urlId = await addUrl(url_example, access_token, request, app);
      return request(app.getHttpServer())
        .post(gql)
        .set('Authorization', `Bearer ${access_token}`)
        .send({
          query: `
          mutation 
          {
            updateUrl(updateUrlInput:
            {
              urlId: "${urlId}",
              name: "catbox_720"
              status: DEAD
            })
            {
              urlId
              songId
              name
              link
              status
            }
          }`,
        })
        .expect((res) => {
          expect(res.body.data.updateUrl).toEqual(
            expect.objectContaining({
              urlId: expect.any(String),
              songId: expect.any(String),
              link: 'https://files.catbox.moe/rrljjp.webm',
              name: 'catbox_720',
              status: 'DEAD',
            }),
          );
        });
    });
  });

  describe('removeUrl', () => {
    it('should remove url object', async () => {
      const animeId = await addAnime(
        ANIME_INPUT_EXAMPLE,
        access_token,
        request,
        app,
      );
      const song_example = {
        ...SONG_INPUT_EXAMPLE,
        animeId: animeId,
      };
      const songId = await addSong(song_example, access_token, request, app);
      const url_example = {
        ...URL_INPUT_EXAMPLE,
        songId: songId,
      };
      const urlId = await addUrl(url_example, access_token, request, app);
      await request(app.getHttpServer())
        .post(gql)
        .set('Authorization', `Bearer ${access_token}`)
        .send({
          query: `
          mutation 
          {
            removeUrl(urlId: "${urlId}")
            {
              urlId
            }
          }`,
        });
      return request(app.getHttpServer())
        .post(gql)
        .send({
          query: `
        query 
        {
          url(urlId: "${urlId}")
          {
            urlId
          }
        }`,
        })
        .expect((res) => {
          expect(res.body.errors).toContainEqual({
            message: BAD_USER_INPUT.URL_NOT_FOUND,
            statusCode: 400,
          });
        });
    });
  });
});
