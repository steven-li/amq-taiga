import { GraphQLError } from 'graphql';
import { ERRORS } from '../constants/errors.constants';

interface ErrorMessage {
  message: string;
  statusCode: number;
}

export function ErrorsFormater(err: GraphQLError): ErrorMessage {
  if (err.extensions.code in ERRORS) {
    if (ERRORS[err.extensions.code].accepted_messages.includes(err.message)) {
      return {
        message: err.message,
        statusCode: ERRORS[err.extensions.code].statusCode,
      };
    } else {
      // console.log(`[Error handler] Message "${err.message}" isn't specified for error code "${err.extensions.code}"`);
      return {
        message: ERRORS[err.extensions.code].default_message,
        statusCode: ERRORS[err.extensions.code].statusCode,
      };
    }
  } else {
    // console.log(`[Error handler] Message "${err.message}" and error code "${err.extensions.code} aren't specified"`);
    return { message: 'Internal error', statusCode: 500 };
  }
}
