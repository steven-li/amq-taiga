import { INestApplication } from '@nestjs/common';
import { LoginUserInput } from '../../users/dto/login-user.input';
import { RegisterUserInput } from '../../users/dto/register-user.input';

export async function getAccessToken(
  user: LoginUserInput,
  request: any,
  app: INestApplication,
): Promise<string> {
  const loginRes = await request(app.getHttpServer())
    .post('/graphql')
    .send({
      query: `
      mutation {
        login(loginUserInput: 
        {
          email: "${user.email}",
          password: "${user.password}"
        })
        {
          access_token
          user {
            email
          }
        }
      }`,
    });
  return loginRes.body.data.login.access_token;
}

export async function register(
  user: RegisterUserInput,
  request: any,
  app: INestApplication,
) {
  await request(app.getHttpServer())
    .post('/graphql')
    .send({
      query: `
      mutation{
        register(registerUserInput: {
          email: "${user.email}"
          pseudo: "${user.pseudo}"
          password: "${user.password}"
        }){
          email
          pseudo
          role
        }
      }`,
    });
}
