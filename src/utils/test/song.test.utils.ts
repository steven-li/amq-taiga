import { INestApplication } from '@nestjs/common';
import { CreateSongInput } from '../../songs/dto/create-song.input';

export async function addSong(
  song: CreateSongInput,
  access_token: string,
  request: any,
  app: INestApplication,
): Promise<string> {
  const songRes = await request(app.getHttpServer())
    .post('/graphql')
    .set('Authorization', `Bearer ${access_token}`)
    .send({
      query: `
      mutation 
      {
        createSong(createSongInput:
        {
          animeId: "${song.animeId}",
          type: ${song.type}
          title: "${song.title}",
          artist: "${song.artist}"
        })
        {
          songId
        }
      }`,
    });
  return songRes.body.data.createSong.songId;
}
