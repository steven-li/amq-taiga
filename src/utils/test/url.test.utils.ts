import { CreateUrlInput } from '../../urls/dto/create-url.input';
import { INestApplication } from '@nestjs/common';

export async function addUrl(
  url: CreateUrlInput,
  access_token: string,
  request: any,
  app: INestApplication,
): Promise<string> {
  const urlRes = await request(app.getHttpServer())
    .post('/graphql')
    .set('Authorization', `Bearer ${access_token}`)
    .send({
      query: `
          mutation 
          {
            createUrl(createUrlInput:
            {
              songId: "${url.songId}",
              name: "${url.name}"
              link: "${url.link}",
            })
            {
              urlId
            }
          }`,
    });
  return urlRes.body.data.createUrl.urlId;
}
