import { INestApplication } from '@nestjs/common';
import { CreateAnimeInput } from '../../animes/dto/create-anime.input';

export async function addAnime(
  anime: CreateAnimeInput,
  access_token: string,
  request: any,
  app: INestApplication,
): Promise<string> {
  const animeRes = await request(app.getHttpServer())
    .post('/graphql')
    .set('Authorization', `Bearer ${access_token}`)
    .send({
      query: `
          mutation 
          {
            createAnime(createAnimeInput:
            {
              romajiName: "${anime.romajiName}"
              englishName: "${anime.englishName}"
              annId: ${anime.annId}
            })
            {
              animeId
            }
          }`,
    });
  return animeRes.body.data.createAnime.animeId;
}
