import { CreateAnimeInput } from '../animes/dto/create-anime.input';
import { CreateSongInput } from '../songs/dto/create-song.input';
import { SongType } from '../songs/entities/song.entity';
import { CreateUrlInput } from '../urls/dto/create-url.input';
import { RegisterUserInput } from '../users/dto/register-user.input';
import { LoginUserInput } from '../users/dto/login-user.input';

export const ANIME_INPUT_EXAMPLE: CreateAnimeInput = {
  romajiName: 'Kimetsu no Yaiba: Yūkaku-hen',
  englishName: 'Demon Slayer: Kimetsu no Yaiba Entertainment District Arc',
  annId: 24043,
};

export const SONG_INPUT_EXAMPLE: CreateSongInput = {
  animeId: '',
  title: 'Zankyou Zanka',
  type: SongType.OP,
  artist: 'Aimer',
};

export const URL_INPUT_EXAMPLE: CreateUrlInput = {
  songId: '',
  name: 'catbox_720',
  link: 'https://files.catbox.moe/rrljjp.webm',
};

export const REGISTER_INPUT_EXAMPLE1: RegisterUserInput = {
  pseudo: 'test',
  email: 'test@test.com',
  password: 'password',
};

export const LOGIN_INPUT_EXAMPLE1: LoginUserInput = {
  email: REGISTER_INPUT_EXAMPLE1.email,
  password: REGISTER_INPUT_EXAMPLE1.password,
};
