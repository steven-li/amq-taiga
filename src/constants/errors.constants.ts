export enum INTERNAL_SERVER_ERROR {
  COMPLEXITY_ERROR = 'Query is too complex.',
}

export enum BAD_USER_INPUT {
  BAD_REQUEST = 'Bad request.',
  ANIME_NOT_FOUND = 'Anime not found.',
  SONG_NOT_FOUND = 'Song not found.',
  URL_NOT_FOUND = 'Url not found.',
  USER_NOT_FOUND = 'User not found.',
  EMAIL_IN_USE = 'Email already in use.',
  EMAIL_NOT_VALID = 'Email is not valid.',
  PASSWORD_NOT_VALID = 'Password is not valid.',
}

export enum UNAUTHORIZED {
  UNAUTHORIZED = 'Unauthorized.',
}

type ErrorType = {
  [key: string]: {
    default_message: string;
    statusCode: number;
    accepted_messages: Array<string>;
  };
};

export const ERRORS: ErrorType = {
  SERVER_ERROR: {
    default_message: 'Server error.',
    statusCode: 500,
    accepted_messages: [],
  },

  INTERNAL_SERVER_ERROR: {
    default_message: 'Server error.',
    statusCode: 500,
    accepted_messages: [INTERNAL_SERVER_ERROR.COMPLEXITY_ERROR],
  },

  GRAPHQL_VALIDATION_FAILED: {
    default_message: 'Your query is not valid.',
    statusCode: 400,
    accepted_messages: [],
  },

  UNAUTHENTICATED: {
    default_message: 'Unauthorized.',
    statusCode: 401,
    accepted_messages: [],
  },

  UNAUTHORIZED: {
    default_message: 'Unauthorized.',
    statusCode: 401,
    accepted_messages: [],
  },

  BAD_USER_INPUT: {
    default_message: 'Bad request.',
    statusCode: 400,
    accepted_messages: [
      BAD_USER_INPUT.ANIME_NOT_FOUND,
      BAD_USER_INPUT.SONG_NOT_FOUND,
      BAD_USER_INPUT.URL_NOT_FOUND,
      BAD_USER_INPUT.USER_NOT_FOUND,
      BAD_USER_INPUT.EMAIL_IN_USE,
      BAD_USER_INPUT.EMAIL_NOT_VALID,
      BAD_USER_INPUT.PASSWORD_NOT_VALID,
    ],
  },
};
