import { Injectable } from '@nestjs/common';
import { CreateUrlInput } from './dto/create-url.input';
import { UpdateUrlInput } from './dto/update-url.input';
import { Url } from './entities/url.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserInputError } from 'apollo-server-core';
import { BAD_USER_INPUT } from '../constants/errors.constants';

@Injectable()
export class UrlsService {
  constructor(@InjectRepository(Url) private urlsRepository: Repository<Url>) {}

  async create(createUrlInput: CreateUrlInput) {
    const url = await this.urlsRepository.save(createUrlInput);
    return url;
  }

  async findAllBySongId(songId: string) {
    return await this.urlsRepository.find({ where: { songId: songId } });
  }

  async findOne(urlId: string) {
    const url = await this.urlsRepository.findOne(urlId);
    if (url === undefined) {
      throw new UserInputError(BAD_USER_INPUT.URL_NOT_FOUND);
    } else {
      return url;
    }
  }

  async update(urlId: string, updateUrlInput: UpdateUrlInput) {
    const url = await this.findOne(urlId);
    return this.urlsRepository.save({
      ...url,
      ...updateUrlInput,
      urlId: url.urlId,
    });
  }

  async remove(urlId: string) {
    const url = await this.findOne(urlId);
    const deleteResponse = await this.urlsRepository.delete(urlId);
    if (!deleteResponse.affected) {
      throw new Error(`Error on url deletion`);
    } else {
      return url;
    }
  }
}
