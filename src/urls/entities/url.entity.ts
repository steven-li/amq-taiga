import { ObjectType, Field, registerEnumType } from '@nestjs/graphql';
import { Song } from '../../songs/entities/song.entity';
import {
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
  Entity,
} from 'typeorm';

export enum LinkStatus {
  DEAD,
  WORK,
}

registerEnumType(LinkStatus, {
  name: 'LinkStatus',
});

@Entity()
@ObjectType()
export class Url {
  @PrimaryGeneratedColumn('uuid')
  @Field(() => String, { description: 'Song url UUID' })
  urlId: string;

  @Column()
  @Field(() => String, { description: 'Url name for reference' })
  name: string;

  @Column()
  @Field(() => String, {
    description: 'Url link. Example: https://files.catbox.moe/rrljjp.webm',
  })
  link: string;

  @Column('int', { default: 1 })
  @Field(() => LinkStatus, { description: 'Tag the url as working or dead' })
  status: LinkStatus;

  @Column()
  @Field(() => String, { description: 'Song UUID' })
  songId: string;

  @ManyToOne(() => Song, (song) => song.urls, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'songId' })
  @Field(() => Song, { description: 'Related song' })
  song: Song;
}
