import { forwardRef, Module } from '@nestjs/common';
import { UrlsService } from './urls.service';
import { UrlsResolver } from './urls.resolver';
import { Url } from './entities/url.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SongsModule } from '../songs/songs.module';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Url]),
    forwardRef(() => SongsModule),
    forwardRef(() => UsersModule),
  ],
  providers: [UrlsResolver, UrlsService],
  exports: [UrlsService],
})
export class UrlsModule {}
