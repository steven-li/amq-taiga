import {
  Resolver,
  Query,
  Mutation,
  Args,
  ResolveField,
  Parent,
} from '@nestjs/graphql';
import { UrlsService } from './urls.service';
import { Url } from './entities/url.entity';
import { CreateUrlInput } from './dto/create-url.input';
import { UpdateUrlInput } from './dto/update-url.input';
import { Role } from '../constants/roles.constants';
import { Authorize } from '../decorators/authorize.decorator';
import { SongsService } from '../songs/songs.service';

@Resolver(() => Url)
export class UrlsResolver {
  constructor(
    private readonly urlsService: UrlsService,
    private readonly songsService: SongsService,
  ) {}

  @Mutation(() => Url)
  @Authorize(Role.USER)
  createUrl(@Args('createUrlInput') createUrlInput: CreateUrlInput) {
    return this.urlsService.create(createUrlInput);
  }

  @Query(() => Url, { name: 'url' })
  findOne(@Args('urlId', { type: () => String }) urlId: string) {
    return this.urlsService.findOne(urlId);
  }

  @Mutation(() => Url)
  @Authorize(Role.USER)
  updateUrl(@Args('updateUrlInput') updateUrlInput: UpdateUrlInput) {
    return this.urlsService.update(updateUrlInput.urlId, updateUrlInput);
  }

  @Mutation(() => Url)
  @Authorize(Role.USER)
  removeUrl(@Args('urlId', { type: () => String }) urlId: string) {
    return this.urlsService.remove(urlId);
  }

  @ResolveField()
  async song(@Parent() url: Url) {
    const { songId } = url;
    return this.songsService.findOne(songId);
  }
}
