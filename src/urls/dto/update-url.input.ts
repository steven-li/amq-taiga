import { CreateUrlInput } from './create-url.input';
import { InputType, Field, PartialType } from '@nestjs/graphql';
import { LinkStatus } from '../entities/url.entity';
import { IsUUID } from 'class-validator';

@InputType()
export class UpdateUrlInput extends PartialType(CreateUrlInput) {
  @Field(() => String)
  @IsUUID()
  urlId: string;

  @Field(() => LinkStatus, { nullable: true })
  status?: LinkStatus;
}
