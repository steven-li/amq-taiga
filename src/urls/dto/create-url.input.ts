import { InputType, Field } from '@nestjs/graphql';
import { IsString, IsUrl, IsUUID } from 'class-validator';

@InputType()
export class CreateUrlInput {
  @Field(() => String)
  @IsString()
  name: string;

  @Field(() => String)
  @IsUrl()
  link: string;

  @Field(() => String)
  @IsUUID()
  songId: string;
}
