import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RegisterUserInput } from './dto/register-user.input';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
  ) {}

  findAll(): Promise<User[]> {
    return this.userRepository.find();
  }

  async findOne(email: string): Promise<User> {
    const user = await this.userRepository.findOne(email);
    return user;
  }

  async register(registerUserInput: RegisterUserInput): Promise<User> {
    const user = await this.userRepository.save(registerUserInput);
    return user;
  }
}
