import { ObjectType, Field, registerEnumType } from '@nestjs/graphql';
import { Entity, Column, PrimaryColumn } from 'typeorm';
import { Anime } from '../../animes/entities/anime.entity';
import { Song } from '../../songs/entities/song.entity';
import { Role } from '../../constants/roles.constants';

registerEnumType(Role, {
  name: 'Role',
  description: 'User roles.',
});

@Entity('users')
@ObjectType()
export class User {
  @Field(() => String, { description: 'User email' })
  @PrimaryColumn()
  email: string;

  @Column()
  password: string;

  @Column()
  @Field(() => String, { description: 'User pseudonyme' })
  pseudo: string;

  @Column('boolean', { default: false })
  @Field(() => Boolean, { description: 'boolean for email verification' })
  verified: boolean;

  @Field(() => [Anime])
  favoriteAnimes?: Array<Anime>;

  @Field(() => [Song])
  favoriteSongs?: Array<Song>;

  @Column('int', { default: Role.USER })
  @Field(() => Role, { description: 'Role of the user' })
  role: Role;
}
