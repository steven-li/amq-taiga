import { Module } from '@nestjs/common';
import { AnimesModule } from './animes/animes.module';
import { GraphQLModule } from '@nestjs/graphql';
import { join } from 'path';
import { SongsModule } from './songs/songs.module';
import { UsersModule } from './users/users.module';
import { UrlsModule } from './urls/urls.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import config from './config/postgre.test.config';
import { DatabaseService } from './database/database.service';
import { ErrorsFormater } from './utils/errors.formater';
import { AuthModule } from './auth/auth.module';
import { TestLogger } from './utils/test.logger';
import { ComplexityPlugin } from './plugins/complexity.plugin';

@Module({
  imports: [
    GraphQLModule.forRoot({
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
      playground: false,
      debug: false,
      formatError: ErrorsFormater,
    }),
    TypeOrmModule.forRoot(config),
    AuthModule,
    AnimesModule,
    SongsModule,
    UsersModule,
    UrlsModule,
  ],
  controllers: [],
  providers: [DatabaseService, TestLogger, ComplexityPlugin],
  exports: [DatabaseService, TestLogger],
})
export class TestModule {}
