import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';
import { Anime } from '../animes/entities/anime.entity';
import { Song } from '../songs/entities/song.entity';
import { Url } from '../urls/entities/url.entity';
import dotenv = require('dotenv');
import { User } from '../users/entities/user.entity';
dotenv.config();

// Jest test needs importation of entities during initialization https://github.com/nestjs/typeorm/issues/9
const entities = [Anime, Song, User, Url];

const config: PostgresConnectionOptions = {
  type: 'postgres',
  host: process.env.POSTGRE_HOST_TEST,
  port: 5432,
  username: process.env.POSTGRE_USERNAME_TEST,
  password: process.env.POSTGRE_PASSWORD_TEST,
  database: process.env.POSTGRE_DATABASE_TEST,
  entities: entities,
  synchronize: true,
};

export default config;
