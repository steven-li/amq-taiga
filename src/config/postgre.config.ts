import dotenv = require('dotenv');
dotenv.config();
import { Anime } from '../animes/entities/anime.entity';
import { Song } from '../songs/entities/song.entity';
import { User } from '../users/entities/user.entity';
import { Url } from 'src/urls/entities/url.entity';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModuleAsyncOptions } from '@nestjs/typeorm';

// Jest test needs importation of entities during initialization https://github.com/nestjs/typeorm/issues/9
const entities = [Anime, Song, User, Url];

export const config: TypeOrmModuleAsyncOptions = {
  imports: [ConfigModule],
  useFactory: (configSerivce: ConfigService) => ({
    type: 'postgres' as const,
    host: configSerivce.get<string>('POSTGRE_HOST'),
    port: parseInt(configSerivce.get<string>('POSTGRE_PORT')),
    username: configSerivce.get<string>('POSTGRE_USERNAME'),
    password: configSerivce.get<string>('POSTGRE_PASSWORD'),
    database: configSerivce.get<string>('POSTGRE_DATABASE'),
    entities: entities,
    synchronize: true,
  }),
  inject: [ConfigService],
};
