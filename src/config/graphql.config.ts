import { GqlModuleAsyncOptions } from '@nestjs/graphql';
import { join } from 'path';
// import { ErrorsFormater } from 'src/utils/errors.formater';

export const config: GqlModuleAsyncOptions = {
  useFactory: () => ({
    autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
    sortSchema: true,
    playground: true,
    debug: false,
    //formatError: ErrorsFormater,
  }),
};
