import { Module } from '@nestjs/common';
import { AnimesModule } from './animes/animes.module';
import { GraphQLModule } from '@nestjs/graphql';
import { SongsModule } from './songs/songs.module';
import { UsersModule } from './users/users.module';
import { UrlsModule } from './urls/urls.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { config as PostgreConfig } from './config/postgre.config';
import { config as GraphQLConfig } from './config/graphql.config';

//import { ErrorsFormater } from './utils/errors.formater';
import { AuthModule } from './auth/auth.module';
import { ComplexityPlugin } from './plugins/complexity.plugin';
@Module({
  imports: [
    GraphQLModule.forRootAsync(GraphQLConfig),
    TypeOrmModule.forRootAsync(PostgreConfig),
    AnimesModule,
    SongsModule,
    UsersModule,
    UrlsModule,
    AuthModule,
  ],
  controllers: [],
  providers: [ComplexityPlugin],
})
export class AppModule {}
