import { GraphQLSchemaHost, Plugin } from '@nestjs/graphql';
import { ApolloServerPlugin } from 'apollo-server-plugin-base';
import { GraphQLError } from 'graphql';
import { INTERNAL_SERVER_ERROR } from '../constants/errors.constants';
import {
  fieldExtensionsEstimator,
  getComplexity,
  simpleEstimator,
} from 'graphql-query-complexity';

@Plugin()
export class ComplexityPlugin implements ApolloServerPlugin {
  constructor(private gqlSchemaHost: GraphQLSchemaHost) {}

  public requestDidStart() {
    const maxComplexity = 20;
    const { schema } = this.gqlSchemaHost;

    return {
      didResolveOperation: ({ request, document }) => {
        const complexity = getComplexity({
          schema,
          operationName: request.operationName,
          query: document,
          variables: request.variables,
          estimators: [
            fieldExtensionsEstimator(),
            simpleEstimator({ defaultComplexity: 1 }),
          ],
        });
        if (complexity > maxComplexity) {
          // #TODO : log complexity (`Query is too complex: ${complexity}. Maximum allowed complexity: ${maxComplexity}`)
          throw new GraphQLError(INTERNAL_SERVER_ERROR.COMPLEXITY_ERROR);
        }
      },
    };
  }
}
