import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';

@Injectable()
export class DatabaseService {
  constructor(private connection: Connection) {}

  private async getRepository<T>(entity): Promise<Repository<T>> {
    return this.connection.getRepository(entity);
  }

  private async getEntities() {
    const entities = [];
    (await (await this.connection).entityMetadatas).forEach((x) =>
      entities.push({ name: x.name, tableName: x.tableName }),
    );
    return entities;
  }

  private async cleanAll(entities) {
    for (const entity of entities) {
      const repository = await this.getRepository(entity.name);
      await repository.query(
        `TRUNCATE ${entity.tableName} RESTART IDENTITY CASCADE;`,
      );
    }
  }

  public async clearDB() {
    const entities = await this.getEntities();
    await this.cleanAll(entities);
  }
}
