import { ObjectType, Field, registerEnumType } from '@nestjs/graphql';
import { Url } from '../../urls/entities/url.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Anime } from '../../animes/entities/anime.entity';

export enum SongType {
  OP = 'OP',
  ED = 'ED',
  INS = 'INS',
}

registerEnumType(SongType, {
  name: 'SongType',
});

@Entity()
@ObjectType()
export class Song {
  @PrimaryGeneratedColumn('uuid')
  @Field(() => String, { description: 'Song UUID' })
  songId: string;

  @Column()
  @Field(() => String, { description: 'Song title' })
  title: string;

  @Column()
  @Field(() => String, { description: 'Artits who participated in the music' })
  artist: string;

  @Column()
  @Field(() => SongType, {
    description:
      'Type of the song in the anime, can be opening, ending or insert ',
  })
  type: SongType;

  @Column()
  @Field(() => String, { description: 'Anime UUID' })
  animeId: string;

  @ManyToOne(() => Anime, (anime) => anime.songs, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'animeId' })
  @Field(() => Anime, { description: 'Anime where the song is played' })
  anime: Anime;

  @OneToMany(() => Url, (url) => url.song)
  @Field(() => [Url], { nullable: true, description: 'Videos url of the song' })
  urls?: Array<Url>;
}
