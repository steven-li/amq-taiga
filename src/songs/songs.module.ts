import { Module, forwardRef } from '@nestjs/common';
import { SongsService } from './songs.service';
import { SongsResolver } from './songs.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Song } from './entities/song.entity';
import { AnimesModule } from '../animes/animes.module';
import { UrlsModule } from '../urls/urls.module';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Song]),
    forwardRef(() => AnimesModule),
    forwardRef(() => UrlsModule),
    forwardRef(() => UsersModule),
  ],
  providers: [SongsResolver, SongsService],
  exports: [SongsService],
})
export class SongsModule {}
