import { CreateSongInput } from './create-song.input';
import { InputType, Field, PartialType } from '@nestjs/graphql';
import { IsUUID } from 'class-validator';
@InputType()
export class UpdateSongInput extends PartialType(CreateSongInput) {
  @IsUUID()
  @Field(() => String)
  songId: string;
}
