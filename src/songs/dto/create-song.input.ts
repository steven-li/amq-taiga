import { InputType, Field } from '@nestjs/graphql';
import { IsString, IsUUID } from 'class-validator';
import { SongType } from '../entities/song.entity';

@InputType()
export class CreateSongInput {
  @Field(() => String)
  @IsString()
  title: string;

  @Field(() => SongType)
  type: SongType;

  @Field(() => String)
  @IsString()
  artist: string;

  @Field(() => String)
  @IsUUID()
  animeId: string;
}
