import {
  Resolver,
  Query,
  Mutation,
  Args,
  ResolveField,
  Parent,
} from '@nestjs/graphql';
import { SongsService } from './songs.service';
import { Song } from './entities/song.entity';
import { CreateSongInput } from './dto/create-song.input';
import { UpdateSongInput } from './dto/update-song.input';
import { AnimesService } from '../animes/animes.service';
import { UrlsService } from '../urls/urls.service';
import { Role } from '../constants/roles.constants';
import { Authorize } from '../decorators/authorize.decorator';

@Resolver(() => Song)
export class SongsResolver {
  constructor(
    private readonly songsService: SongsService,
    private readonly animesService: AnimesService,
    private readonly urlsService: UrlsService,
  ) {}

  @Mutation(() => Song)
  @Authorize(Role.USER)
  createSong(@Args('createSongInput') createSongInput: CreateSongInput) {
    return this.songsService.create(createSongInput);
  }

  /*
  @Query(() => [Song], { name: 'songs' })
  findAll() {
    return this.songsService.findAll();
  }*/

  @Query(() => Song, { name: 'song' })
  findOne(@Args('songId', { type: () => String }) songId: string) {
    return this.songsService.findOne(songId);
  }

  @Mutation(() => Song)
  @Authorize(Role.USER)
  updateSong(@Args('updateSongInput') updateSongInput: UpdateSongInput) {
    return this.songsService.update(updateSongInput.songId, updateSongInput);
  }

  @Mutation(() => Song)
  @Authorize(Role.USER)
  removeSong(@Args('songId', { type: () => String }) songId: string) {
    return this.songsService.remove(songId);
  }

  @ResolveField()
  async anime(@Parent() song: Song) {
    const { animeId } = song;
    return this.animesService.findOne(animeId);
  }

  @ResolveField()
  async urls(@Parent() song: Song) {
    const { songId } = song;
    return this.urlsService.findAllBySongId(songId);
  }
}
