import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserInputError } from 'apollo-server-core';
import { Repository } from 'typeorm';
import { CreateSongInput } from './dto/create-song.input';
import { UpdateSongInput } from './dto/update-song.input';
import { Song } from './entities/song.entity';
import { BAD_USER_INPUT } from '../constants/errors.constants';

@Injectable()
export class SongsService {
  constructor(
    @InjectRepository(Song) private songsRepository: Repository<Song>,
  ) {}

  async create(createSongInput: CreateSongInput): Promise<Song> {
    const song = await this.songsRepository.save(createSongInput);
    return song;
  }

  async findAllByAnimeId(animeId: string) {
    return await this.songsRepository.find({ where: { animeId: animeId } });
  }

  async findOne(songId: string) {
    const song = await this.songsRepository.findOne(songId);
    if (song === undefined) {
      throw new UserInputError(BAD_USER_INPUT.SONG_NOT_FOUND);
    } else {
      return song;
    }
  }

  async update(songId: string, updateSongInput: UpdateSongInput) {
    const song = await this.findOne(songId);
    return this.songsRepository.save({
      ...song,
      ...updateSongInput,
      songId: song.songId,
    });
  }

  async remove(songId: string) {
    const song = await this.findOne(songId);
    const deleteResponse = await this.songsRepository.delete(songId);
    if (!deleteResponse.affected) {
      throw new Error(`Error on song deletion`);
    } else {
      return song;
    }
  }
}
