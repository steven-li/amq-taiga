import { Injectable } from '@nestjs/common';
import { RegisterUserInput } from 'src/users/dto/register-user.input';
import { User } from '../users/entities/user.entity';
import { UsersService } from '../users/users.service';
import * as bcrypt from 'bcrypt';
import { UserInputError } from 'apollo-server-core';
import { JwtService } from '@nestjs/jwt';
import { BAD_USER_INPUT } from '../constants/errors.constants';
@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, password: string): Promise<any> {
    const user = await this.usersService.findOne(email);
    if (!user) {
      throw new UserInputError(BAD_USER_INPUT.EMAIL_NOT_VALID);
    }
    const valid = await bcrypt.compare(password, user.password);
    if (valid) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { password, ...result } = user;
      return result;
    } else {
      throw new UserInputError(BAD_USER_INPUT.PASSWORD_NOT_VALID);
    }
  }

  async login(user: User) {
    const token = {
      access_token: this.jwtService.sign({
        username: user.pseudo,
        email: user.email,
      }),
      user,
    };
    return token;
  }

  async register(registerUserInput: RegisterUserInput) {
    const user = await this.usersService.findOne(registerUserInput.email);

    if (user) {
      throw new UserInputError(BAD_USER_INPUT.EMAIL_IN_USE);
    }
    const password = await bcrypt.hash(registerUserInput.password, 10);
    return this.usersService.register({
      pseudo: registerUserInput.pseudo,
      email: registerUserInput.email,
      password,
    });
  }
}
