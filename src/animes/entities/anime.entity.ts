import { ObjectType, Field, Int } from '@nestjs/graphql';
import { Song } from '../../songs/entities/song.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
@Entity()
@ObjectType()
export class Anime {
  @PrimaryGeneratedColumn('uuid')
  @Field(() => String, { description: 'Anime UUID' })
  animeId: string;

  @Column()
  @Field(() => String, { description: 'Anime name in romaji' })
  romajiName: string;

  @Column()
  @Field(() => String, { description: 'Anime name in english' })
  englishName: string;

  @Column()
  @Field(() => Int, {
    nullable: true,
    description:
      'AnimeNewsNetwork Id, https://www.animenewsnetwork.com/encyclopedia/anime.php?id=<annId>',
  })
  annId: number;

  @Column({ nullable: true })
  @Field(() => Int, {
    nullable: true,
    description: 'Anilist Id, https://anilist.co/anime/112719/<anilistId>',
  })
  anilistId?: number;

  @Column({ nullable: true })
  @Field(() => Int, {
    nullable: true,
    description: 'MyAnimeList Id,https://myanimelist.net/anime/<malId>',
  })
  malId?: number;

  @Column({ nullable: true })
  @Field(() => Int, { nullable: true })
  kitsuId?: number;

  @OneToMany(() => Song, (song) => song.anime)
  @Field(() => [Song], { nullable: true, description: 'Anime related songs' })
  songs?: Array<Song>;
}
