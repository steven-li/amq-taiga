import { CreateAnimeInput } from './create-anime.input';
import { InputType, Field, PartialType } from '@nestjs/graphql';
import { IsUUID } from 'class-validator';
@InputType()
export class UpdateAnimeInput extends PartialType(CreateAnimeInput) {
  @IsUUID()
  @Field(() => String)
  animeId: string;
}
