import { InputType, Int, Field } from '@nestjs/graphql';
import { IsInt, IsString } from 'class-validator';

@InputType()
export class CreateAnimeInput {
  @Field(() => String)
  @IsString()
  romajiName: string;

  @Field(() => String)
  @IsString()
  englishName: string;

  @Field(() => Int)
  @IsInt()
  annId: number;

  @Field(() => Int, { nullable: true })
  anilistId?: number;

  @Field(() => Int, { nullable: true })
  malId?: number;

  @Field(() => Int, { nullable: true })
  kitsuId?: number;
}
