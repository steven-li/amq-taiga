import {
  Resolver,
  Query,
  Mutation,
  Args,
  ResolveField,
  Parent,
} from '@nestjs/graphql';
import { Request } from '@nestjs/common';
import { AnimesService } from './animes.service';
import { SongsService } from '../songs/songs.service';
import { Anime } from './entities/anime.entity';
import { CreateAnimeInput } from './dto/create-anime.input';
import { UpdateAnimeInput } from './dto/update-anime.input';
import { Role } from '../constants/roles.constants';
import { Authorize } from '../decorators/authorize.decorator';

@Resolver(() => Anime)
export class AnimesResolver {
  constructor(
    private readonly animesService: AnimesService,
    private readonly songsService: SongsService,
  ) {}

  @Mutation(() => Anime)
  @Authorize(Role.USER)
  createAnime(
    @Request() req,
    @Args('createAnimeInput') createAnimeInput: CreateAnimeInput,
  ) {
    return this.animesService.create(createAnimeInput);
  }

  @Query(() => [Anime], { name: 'animes' })
  findAll() {
    return this.animesService.findAll();
  }

  @Query(() => Anime, { name: 'anime' })
  findOne(@Args('animeId') animeId: string) {
    return this.animesService.findOne(animeId);
  }

  @Mutation(() => Anime)
  @Authorize(Role.USER)
  updateAnime(@Args('updateAnimeInput') updateAnimeInput: UpdateAnimeInput) {
    return this.animesService.update(
      updateAnimeInput.animeId,
      updateAnimeInput,
    );
  }

  @Mutation(() => Anime)
  @Authorize(Role.USER)
  removeAnime(@Args('animeId', { type: () => String }) animeId: string) {
    return this.animesService.remove(animeId);
  }

  @ResolveField()
  async songs(@Parent() anime: Anime) {
    const { animeId } = anime;
    return this.songsService.findAllByAnimeId(animeId);
  }
}
