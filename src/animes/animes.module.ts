import { forwardRef, Module } from '@nestjs/common';
import { AnimesService } from './animes.service';
import { AnimesResolver } from './animes.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Anime } from './entities/anime.entity';
import { SongsModule } from '../songs/songs.module';
import { UsersModule } from '../users/users.module';
@Module({
  imports: [
    TypeOrmModule.forFeature([Anime]),
    forwardRef(() => SongsModule),
    forwardRef(() => UsersModule),
  ],
  providers: [AnimesResolver, AnimesService],
  exports: [AnimesService],
})
export class AnimesModule {}
