import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserInputError } from 'apollo-server-core';
import { Repository } from 'typeorm';
import { CreateAnimeInput } from './dto/create-anime.input';
import { UpdateAnimeInput } from './dto/update-anime.input';
import { Anime } from './entities/anime.entity';
import { BAD_USER_INPUT } from '../constants/errors.constants';

@Injectable()
export class AnimesService {
  constructor(
    @InjectRepository(Anime) private animesRepository: Repository<Anime>,
  ) {}

  async create(createAnimeInput: CreateAnimeInput): Promise<Anime> {
    const anime = await this.animesRepository.save(createAnimeInput);
    return anime;
  }

  findAll(): Promise<Anime[]> {
    return this.animesRepository.find();
  }

  async findOne(animeId: string): Promise<Anime> {
    const anime = await this.animesRepository.findOne(animeId);
    if (anime === undefined) {
      throw new UserInputError(BAD_USER_INPUT.ANIME_NOT_FOUND);
    } else {
      return anime;
    }
  }

  async update(animeId: string, updateAnimeInput: UpdateAnimeInput) {
    const anime = await this.findOne(animeId);
    return this.animesRepository.save({
      ...anime,
      ...updateAnimeInput,
      animeId: anime.animeId,
    });
  }

  async remove(animeId: string) {
    const anime = await this.findOne(animeId);
    const deleteResponse = await this.animesRepository.delete(animeId);
    if (!deleteResponse.affected) {
      throw new Error(`Error on anime deletion`);
    } else {
      return anime;
    }
  }
}
