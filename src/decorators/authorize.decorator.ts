import { applyDecorators, SetMetadata, UseGuards } from '@nestjs/common';
import { RolesGuard } from '../guards/roles.guard';
import { JwtAuthGuard } from '../guards/jwt-auth.guard';

export const ROLE_KEY = 'roles';

export const Authorize = (roles?: number | number[]) => {
  return applyDecorators(
    SetMetadata('roles', [roles].flat()),
    UseGuards(JwtAuthGuard, RolesGuard),
  );
};
