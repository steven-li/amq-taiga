# AMQ Taiga

The stack is made up of:
- [**N**estjs](https://nestjs.com/)(version. 8.0.0) : A progressive Node.js framework
- [**P**ostgreSQL](https://www.postgresql.org): For persistent data, we store it in PostgreSQL.
### Pre-requisites
* git - [Installation guide](https://www.linode.com/docs/development/version-control/how-to-install-git-on-linux-mac-and-windows/)
* node.js (current version used: 16.10.0)  [Download page](https://nodejs.org/en/download/)

> **Tips**: For node.js version, use [Node version manager](https://github.com/nvm-sh/nvm) to configure your environment with the right version.

* PostgreSQL [Download page](https://www.postgresql.org/download/)

* Environment variables: it needs 2 database depending if you run tests.
 ```ascii
 .env
 └── JWT_SECRET
    ├── token for authentification system

 └── POSTGRE_HOST
 └── POSTGRES_PORT
 └── POSTGRE_USERNAME
 └── POSTGRES_PASSWORD
 └── POSTGRE_DATABASE
    ├── database for production or development purposes

 └── POSTGRE_HOST_TEST
 └── POSTGRES_PORT_TEST
 └── POSTGRE_USERNAME_TEST
 └── POSTGRES_PASSWORD_TEST
 └── POSTGRE_DATABASE_TEST
    ├── database for testing purposes

 ```
> We provide `example.env` to prepare easier your own environment.
### Installation
```
npm install
```

### Running the app

- Run `npm start` to start the application.

- Run `npm start:dev` to start the application using [watch mode](https://docs.nestjs.com/recipes/hot-reload).

- Run `npm start:debug` to start the application for  [debugging](https://nodejs.org/en/docs/guides/debugging-getting-started/).

- Run `npm start:prod` to start the application for  [production](https://nodejs.org/en/docs/guides/debugging-getting-started/).
  
### Test 

- Run `npm run test:e2e` to launch end-to-end tests
- Run `npm run test:cov` to launch test coverage

### Build
- Run `npm run build` to build the application

### Linter
- Run `npm run lint` to lint the application using [eslint](https://eslint.org/)

### [Git hooks](https://git-scm.com/docs/githooks)
**commit-msg** : check commit message using [commitlint](https://commitlint.js.org/#/)
> **Commitlint** uses Angular convention, checkout these documentations: [commitlint](https://github.com/conventional-changelog/commitlint), [conventional commits](https://www.conventionalcommits.org/en/v1.0.0-beta.4/).


# Checkpoints report for the project

You **MUST** append a filled copy of this document at the end of your `README.MD`.

This document serves three main purposes:
- providing you a clear list of my expectations (check each point when done) ;
- ensuring I do not miss some of your engineering during the review ;
- asking for additional information that helps me during the review.

## Notice

Check every applicable checkbox in the above list. For each one, provide the requested additional information.

In your explanation, please provide links (file + line) to relevant parts of your source code and tests if applicable.

### Caption

🔵 means the checkbox is mandatory. If missing or absolutely not satisfying, it may cost you 0.5 penalty point.

## Expectations

### GraphQL API only

- [x] Reduce code duplication for the various involved schemas (of the database, of the ORM, of GraphQL...). **[1 point]** 🔵
> How did you achieve this?

>-  Nestjs allows us to build our GraphQL application with **code first** method. Our schema and PostgreSQL entities are generated througth the same class by using decorators.
>- For DTO schema, reduce code duplication is applied using utility types (*PartialType* for instance) from "@nestjs/graplql" package.

- [x] Mitigation(s) against too complex GraphQL queries, arbitrary deep nested object fetching or related DoS. **[1 point per mitigation, up to 2]**
> Quote and explain each mitigation.

>- Using complexity approach: For each field, we define a complexity depending of how the field is resolved. The query complexity is computed depending on the complexity attribution. If the number is higher than our maximun complexity, the query will not be executed.
>- A test for complexity is written in `./test/miscellaneous.e2e.spec.ts`.


- [ ] Any security or performance improvement related to your GraphQL implementation, as optionally highlighted in the subject? points]**
> Explain each improvement.

### Input validation

- [x] Strictly and deeply validate the type of every input (`params, querystring, body`) at runtime before any processing. **[1 point]** 🔵
> How did you achieve this?

>- GraphQL allow us to use its own input validation.
>- To complete input validation, we choose to use [class-validator](https://www.npmjs.com/package/class-validator) package to check email, uuid, link.
  
- [x] Ensure the type of every input can be inferred by Typescript at any time and properly propagates across the app. **[1 point]** 🔵
> How did you achieve this?

>- Every input are defined in DTO folder using class and each property have a type. when a property change type, it will propagate and we will receive error in resolvers and services where they are mostly used.

- [ ] Ensure the static and runtime input types are always synced. **[1 point]** 🔵
> How did you achieve this? If extra commands must be run before the typescript checking, how do you ensure there are run?

### Authorisation

- [x] Check the current user is allowed to call this endpoint. **[1 point]** 🔵
> How did you achieve this?
- We use `AuthGuard` to protect our mutation requests. 
  By adding `JwtAuthGuard` decorator to our resolvers methods, we denied the access from unauthenticated user.

- [x] Check the current user is allowed to perform the action on a specific resource. **[1 point]** 🔵
> How did you achieve this?
 
>- We use `Authorize` decorator to protect our mutation request. The approach is to pass the role and it activate 2 guards sequentially. The first one use `JwtAuthGuard` (for authentification) and the second is `RolesGuard` (for authorization) which will take the token payload to retrieve user email and get his role through `UsersService`. We finally check if the user had access to know if we denied the ressource.

- [x] Did you build or use an authorisation framework, making the authorisation widely used in your code base? **[1 point]**
> How did you achieve this?

>- We build our authorisation logic.
>- We wrote our `RolesGuard` to be use as the same as `JwtAuthGuard`.

- [ ] Do you have any way to ensure authorisation is checked on every endpoint? **[1 point]**
> It is pretty easy to forget authorising some action.
> For obvious reasons, it may lead to security issues and bugs.
> At work, we use `varvet/pundit` in our `Ruby on Rails` stack. It can raise exception just before answering the client if authorisation is not checked.
> https://github.com/varvet/pundit#ensuring-policies-and-scopes-are-used
> 
> How did you achieve this?

### Secret and configuration management

- [x] Use a hash for any sensitive data you do not need to store as plain text. 🔵
> Also check this if you do not store any password or such data (and say it here).

- Password are hashed is **bcript** package.
- [x] Store your configuration entries in environment variables or outside the git scope. **[1 point]** 🔵
> How did you achieve this?
- Environment variables are save in `.env` file and only an example of this one are pushed on git.
- Environment variables are retrived using **dotenv** package.
- [x] Do you provide a way to list every configuration entries (setup instructions, documentation, requireness... are appreciated)? **[1 point]**
> How did you achieve this?

>- The readme is enough to config your own instance.

- [ ] Do you have a kind of configuration validation with meaningful error messages? **[1 point]**
> How did you achieve this?

### Package management

- [x] Do not use any package with less than 50k downloads a week. 🔵

- [ ] Did you write some automated tools that check no unpopular dependency was installed? If yes, ensure it runs frequently. **[1 point]**
> How did you achieve this? A Github Action (or similar) and compliance rule for pull requests are appreciated.

- [x] Properly use dependencies and devDevepencies in your package.json. **[0.5 points]**
> How did you achieve this?

>- We checked the scope of each packages. For testing, linting, we placed packages into devDveepencies.
### Automated API generation

- [x] Do you have automated documentation generation for your API (such as OpenAPI/Swagger...)? **[1 point]** 🔵
> How did you achieve this?
> You must link your documentation for review (a Github page, a ZIP archive, an attachment to the release notes...).

>- The playground of graphql is enough for documentation.
>- We found different ways to generated static documentation ([dociql](https://www.npmjs.com/package/dociql), [spectaQL](https://www.npmjs.com/package/spectaql), [docusaurus](https://www.npmjs.com/package/@edno/docusaurus2-graphql-doc-generator)) but they not fullfill the 50k downloads condition.

- [x] In addition to requireness and types, do you provide a comment for every property of your documentation? **[1 point]**
> How did you achieve this?

>- The playground of graphql is generated based of information we put into our entities. We provide a description for mostly all properties.

- [ ] Do you document the schema of responses (at least for success codes) and provide examples of payloads? **[1 point]**
> How did you achieve this?
****
- [ ] Is your documentation automatically built and published when a commit reach the develop or master branches? **[1 point]**
> How did you achieve this?

### Error management

- [x] Do not expose internal application state or code (no sent stacktrace in production!). **[1 point]** 🔵
> How did you achieve this?

> - Every thrown errors are rendered using ErrorFormator (`./src/utils/error.formator.ts`).
> - We specify a better output for the error to not reveal the default stack.

- [ ] Do you report errors to Sentry, Rollbar, Stackdriver… **[1 point]**
> How did you achieve this?

### Log management

- [ ] Mention everything you put in place for a better debugging experience based on the logs collection and analysis. **[3 points]**
> How did you achieve this?

- [ ] Mention everything you put in place to ensure no sensitive data were recorded to the log. **[1 point]**
> How did you achieve this?

### Asynchronous first

- [x] Always use the async implementations when available. **[1 point]** 🔵
> List all the functions you call in their async implementation instead of the sync one.
> 
> Ex: I used `await fs.readFile` in file `folder/xxx.ts:120` instead of `fs.readFileSync`.

>- ``TypeOrmModule.forRootAsync`` instead of ``TypeOrmModule.forRoot``
>- ``GraphQLModule.forRootAsync`` instead of ``GraphQLModule.forRoot``
- [ ] No unhandled promise rejections, no uncaught exceptions… **[1 point]** 🔵
> For example, how do you ensure every promise rejection is caught and properly handled?
> Tips: one part of the answer could be the use of a linter.

### Code quality

- [x] Did you put a focus on reducing code duplication? **[1 point]**
> How did you achieve this?

>- Code redundancy was reduce by using 
>     - constants for tests, error messages. (`./src/constants/`)
>     - test requests using utils to make them (`./src/utils/test/`)

- [ ] Eslint rules are checked for any pushed commit to develop or master branch. **[1 point]**
> Please provide a link to the sample of Github Action logs (or similar).

### Automated tests

- [x] You implemented automated specs. **[1 point]** 🔵
> Please provide a link to the more complete summary you have.

[link](https://efrei365net-my.sharepoint.com/:i:/g/personal/steven_li_efrei_net/EYXx1IGWxMlCnP3vidCpZ8kBdVpOJkXQTdL8r7RWFMwBFg?e=tL3jOq)
- [x] Your test code coverage is 75% or more.  **[1 point]** 🔵
> Please provide a link to the `istanbul` HTML coverage summary (or from a similar tool).

[link](https://efrei365net-my.sharepoint.com/:u:/g/personal/steven_li_efrei_net/EaJNGrqUEipIl0z-FoStd68BZyeC5Aa3Mb95qaUHgp-5pw?e=ExXpai)
- [ ] Do you run the test on a CD/CI, such as Github Action? **[1 point]**
> Please provide a link to the latest test summary you have, hosted on Github Action or similar.

- We got an unsolved problem to connect postgres docker service to our application through gitlab ci