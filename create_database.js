const { Client } = require('pg');
const dotenv = require('dotenv');
dotenv.config();
const client = new Client({
  host: process.env.POSTGRE_HOST_TEST,
  user: process.env.POSTGRE_USERNAME_TEST,
  password: process.env.POSTGRE_PASSWORD_TEST,
  port: 5432,
});

const createDatabase = async () => {
  try {
    await client.connect();
    await client.query(`CREATE DATABASE e2e_test`);
    return true;
  } catch (error) {
    console.error(error.stack);
    return false;
  } finally {
    await client.end();
  }
};

createDatabase().then((result) => {
  if (result) {
    console.log('Database created');
  }
});
